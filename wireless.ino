/* AstroPointer 
 * main.ino
 * 03/09/2021
 * Jacopo Moioli
 */

#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>


const char* NET_SSID = "";
const char* NET_PSW = "";
int PORT = 42069;
int ACTIVATION_BUTTON_PIN = 13;
int RIGHT_CLICK_PIN = 14;
int LEFT_CLICK_PIN = 12;

Adafruit_MPU6050 mpu;
WiFiServer server(PORT);

void setup(void) {
  Serial.begin(115200);
  Serial.setTimeout(1);
  
  pinMode(ACTIVATION_BUTTON_PIN, INPUT_PULLUP); // HIGH when not pressed
  pinMode(RIGHT_CLICK_PIN, INPUT_PULLUP);
  pinMode(LEFT_CLICK_PIN, INPUT_PULLUP);

  // WiFi connection
  WiFi.begin(NET_SSID, NET_PSW);
  while(WiFi.status () != WL_CONNECTED) {
    Serial.println("connecting...");
    delay(1000);
  }
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());
  
  while(!Serial) {
    delay(10);
  }

  if (!mpu.begin()) {
    Serial.println("Gyroscope unit not found. Please check connections");
    while (1) {
      delay(10);
    }
  }
  
  mpu.setAccelerometerRange(MPU6050_RANGE_16_G);
  mpu.setGyroRange(MPU6050_RANGE_250_DEG);
  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
  Serial.println("");
  delay(100);
  server.begin();
}

void loop() {
  WiFiClient client = server.available();
  Serial.println("Listening for client");

  if(client){
      while(client.connected()){
        for(int i=0; i<6; i++){
          work_loop(client); // check if still connected every 5 loops in order to decrease latency
        }
      }
      client.stop();
  }
  delay(10);
  
}

void work_loop(WiFiClient client) {
  if (digitalRead(RIGHT_CLICK_PIN)==LOW){
      Serial.println("RCLICK");
      client.write("RCLICK");
      while(digitalRead(RIGHT_CLICK_PIN)==LOW){
        delay(10);
      }
    }

    if (digitalRead(LEFT_CLICK_PIN)==LOW){
      Serial.println("LCLICK");
      client.write("LCLICK");  
      while(digitalRead(LEFT_CLICK_PIN)==LOW){
        delay(10);
      }
    }

    if(digitalRead(ACTIVATION_BUTTON_PIN)==LOW){   
      sensors_event_t a, g, thermo;
      mpu.getEvent(&a, &g, &thermo);
  
      String reading = ":"+String(g.gyro.z) + "," + String(a.acceleration.x) + "," + String(a.acceleration.y) + "," + String(a.acceleration.z);
      char charReading[100];
      Serial.println(reading);
      reading.toCharArray(charReading, 100);
      client.write(charReading);
    }
    delay(10);
}
