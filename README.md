# Astropointer
#### A Gyroscope pointer device

## What is this
A "device" that uses a gyroscope and accelerometer to move the cursor, acting as a mouse while looking like a baseless joystick
![demo](https://jacopomoioli.xyz/videos/demos/astropointer_demo_1.mp4)

## How to use it
- Flash "main.ino" on your ESP8266
- Change the port on reciver.py
- Run reciver.py (root privileges could be needed)
- ???
- Profit

## Components
- 1 NodeMCU Amica ESP 8266
- MPU6050
- Jumper Cables
- USB cable

## Features
- Activation button
- Left and Right click Button
- WiFi transmission


## Future Features
There is a huge room for improvements. For example:
- Drag support
- Scroll support
- Reduce latency
- Increase stability while increasing sensitivity
- use arduino mouse lib or write it on my own instead of reciver script


