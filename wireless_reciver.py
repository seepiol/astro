import serial
import pyautogui
import time
import sys
import socket
import sys

MUL_FACTOR_PITCH=100
MUL_FACTOR_YAW=100
PITCH_MAX=10
YAW_MAX=10
THRESHOLD=5
USE_THRESHOLD=False
MAKE_MOVEMENTS=True


class Inclination():
    def __init__(self, pitch, yaw, vert, roll):
        self.pitch = round(float(pitch), 3)
        self.yaw = round(float(yaw)+11, 3) # correct -11 when neutral
        self.vert = float(vert)
        self.roll = float(roll)

def right_click():
    print("right")
    pyautogui.click(button='right')

def left_click():
    print("left")
    pyautogui.click(button='left')

def get_current_inclination():
    try:
        current = s.recv(4096).decode()
        if current == "RCLICK":
            right_click()
            return None
        elif current == "LCLICK":
            left_click()
            return None
        current = current.split(':')[-1] # if more than one position, get only the last one
                                         # less fluid movements but less latency
        current = current.split(',')
    except UnicodeDecodeError:
        print("Error: Check device connections")
        time.sleep(2)
        return None
    except serial.SerialException:
        print("Error: The device has been disconnected or another program is using it")
        time.sleep(2)
        return None


    print(current)

    try:
        return Inclination(current[1], current[2], current[3], current[0])
    except:
        return None

def calculate_next_position(current_p, next_p):
    '''
    if abs((current_p.x-10)/100 - next_p.yaw) > threshold or abs((current_p.y/100) - next_p.pitch) > threshold:
        return current_p.x, current_p.y
    else:
    '''
    
    x = (next_p.yaw*MUL_FACTOR_YAW)+screen.width/2 
    y = (next_p.pitch*MUL_FACTOR_PITCH)+screen.height/2
    
    if USE_THRESHOLD == True:
        if abs(current_p.x - x) < THRESHOLD and abs(current_p.y - y) < THRESHOLD:
            return None, None
        elif abs(current_p.x - x) < 5:
            return current_p.x, y
        elif abs(current_p.y - y) < 5:
            return x, current_p.y
    
    print(f"{x:}, {y:}")
    
    return x, y

def calculate_mul_factor(screen):
    global PITCH_MAX
    global YAW_MAX
    global MUL_FACTOR_PITCH
    global MUL_FACTOR_YAW
    
    MUL_FACTOR_PITCH = (screen.height/2)/PITCH_MAX
    MUL_FACTOR_YAW = 10+(screen.width/2)/YAW_MAX

    

def make_movement(coords):
    current_position = pyautogui.position()
    x, y = calculate_next_position(current_position, coords)
    pyautogui.moveTo(x, y, duration=0.01)


if __name__ == '__main__':
    if len(sys.argv)!=3:
        print("Please provide address and port (example: python3 reciver.py 192.168.1.69 42069)")
        exit()
    
    address = sys.argv[1]
    port = int(sys.argv[2])

    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((address, port))
    except:
        print("Cannot create connection to "+address)
        exit()

    screen = pyautogui.size()
    calculate_mul_factor(screen)
    pyautogui.FAILSAFE=False
    
    print(screen)

    while True:
        coords = get_current_inclination()
        if coords:
            if MAKE_MOVEMENTS:
                make_movement(coords)
            

