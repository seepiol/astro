/* AstroPointer 
 * serial.ino
 * 03/09/2021
 * Jacopo Moioli
 */

#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>

int ACTIVATION_BUTTON_PIN = 13;
int RIGHT_CLICK_PIN = 14;
int LEFT_CLICK_PIN = 12;

Adafruit_MPU6050 mpu;

void setup(void) {
  Serial.begin(115200);
  Serial.setTimeout(1);
  pinMode(ACTIVATION_BUTTON_PIN, INPUT_PULLUP); // HIGH when not pressed
  pinMode(RIGHT_CLICK_PIN, INPUT_PULLUP);
  pinMode(LEFT_CLICK_PIN, INPUT_PULLUP);

  while(!Serial) {
    delay(10);
  }

  if (!mpu.begin()) {
    Serial.println("Gyroscope unit not found. Please check connections");
    while (1) {
      delay(10);
    }
  }
  
  mpu.setAccelerometerRange(MPU6050_RANGE_16_G);
  mpu.setGyroRange(MPU6050_RANGE_250_DEG);
  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
  Serial.println("");
  delay(100);
}

void loop() {

  // check if right click button is clicked
  if (digitalRead(RIGHT_CLICK_PIN)==LOW){
    Serial.println("RCLICK");
    while(digitalRead(RIGHT_CLICK_PIN)==LOW){
      
    }
  }
  if (digitalRead(LEFT_CLICK_PIN)==LOW){
    Serial.println("LCLICK");  
    while(digitalRead(LEFT_CLICK_PIN)==LOW){
      
    }
  }

  if(digitalRead(ACTIVATION_BUTTON_PIN)==LOW){   
    sensors_event_t a, g, thermo;
    mpu.getEvent(&a, &g, &thermo);

  /* Values serial output
   * 
   * Format:
   * Roll, Pitch, Yaw, Vertical
   */
  
  // roll
    Serial.print(g.gyro.z);
    Serial.print(",");

  // pitch
    Serial.print(a.acceleration.x);
    Serial.print(",");

  // yaw
    Serial.print(a.acceleration.y);
    Serial.print(",");

  // vertical accel
    Serial.print(a.acceleration.z);
    Serial.print(",");
    Serial.println("");
  } 

}
